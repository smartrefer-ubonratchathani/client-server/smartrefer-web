<div class="wrapper" [style.background-image]="'url(../assets/images/lotus.jpg)'"
    [style.background-repeat]="'repeat'"

    [style.width.px]='scrWidth' [style.height.px]='scrHeight'>

    <div class="flex card-container indigo-container">
        <div class="hidden lg:inline-flex flex-1 h-4rem text-white font-bold text-center p-4 border-round"></div>
        
        <div class="flex-1 h-4rem text-white font-bold text-center p-4 border-round">

            <div class="shadow-8 border-round  pt-6 pb-6"
            style="background: rgb(2,13,20);
            background: linear-gradient(90deg, rgba(2,13,20,0.5718662464985995) 0%, rgba(11,28,23,0.5970763305322129) 49%, rgba(8,22,48,0.5578606442577031) 100%);"
                    >
                <!-- <p-card header="" subheader="" styleClass="p-card-shadow" (keyup.enter)="onLogin()"> -->
                <div class="pt-0 pb-0" >
                    <span>
                        <p-button styleClass="p-button-outlined">
                            <img alt="logo" src="assets/images/s.png" style="width: 2.5rem"/>
                            <span class="ml-2 font-bold text-xl">SMART REFER </span>
                        </p-button>
                    </span>
                    <br>
                    <br>
                    <span><h1>กรุณาล็อกอินเข้าใช้งานระบบ</h1></span>
                    <br>
                    <div class="p-fluid">
                        <div class="col-12 pr-8 pl-8 mb-2">
                            <h3>ชื่อผู้ใช้</h3>
                            <div class="p-inputgroup p-col-12 p-md-10 mt-2">
                                <span class="p-inputgroup-addon"><i class="pi pi-user"></i></span>
                                <input id="username" type="text" pInputText placeholder="Username"  [(ngModel)]="username" class="p-inputtext-lg">         
                            </div>
                        </div>
                        <!-- <div class="p-field p-grid pr-8 pl-8 mb-4">
                            <label for="username" class="p-col-12 mb-2 pb-5">ชื่อผู้ใช้</label>
                            <div class="p-col-12 p-md-10 mt-2">
                                <input id="username" [(ngModel)]="username" type="text" pInputText>
                            </div>
                        </div> -->
                        <div class="col-12 pr-8 pl-8 mb-6">
                            <h3>รหัสผ่าน</h3>
                            <div class="p-inputgroup p-col-12 p-md-10 mt-2">
                                <span class="p-inputgroup-addon"><i class="pi pi-key"></i></span>
                                <input id="password" [(ngModel)]="password" type="password" pInputText class="p-inputtext-lg"  (keyup.enter)="onLogin()">         
                            </div>
                        </div>
                        <!-- <div class="p-field p-grid pr-8 pl-8 mb-4 ">
                            <label for="password" class="p-col-12 p-mb-2 p-md-2 p-mb-md-0">รหัสผ่าน</label>
                            <div class="p-col-12 p-md-10 mt-2">
                                <input id="password" [(ngModel)]="password" type="password" pInputText>
                            </div>
                        </div> -->
                        <br>
                        <div class="p-field p-grid pr-8 pl-8">
                            <!-- <div class="p-col-12 p-md-10"> -->
                                <!-- <button pButton type="button" (click)="onLogin()" (keyup.enter)="onLogin()" label="เข้าระบบ"></button> -->
                                <button pButton pRipple type="button" (click)="onLogin()" (keyup.enter)="onLogin()" label="เข้าสู่ระบบ" class="p-button-rounded p-button-info text-xl">
                                    <button pButton pRipple type="button" icon="pi pi-check" class="p-button-rounded"></button>
                                </button>
                            <!-- </div> -->
                        </div>

                    </div>
                </div>
                <!-- </p-card> -->

            </div>


        </div>

        <div class="hidden lg:inline-flex flex-1 h-4rem text-white font-bold text-center p-4 border-round mx-4"></div>
    </div>

</div>
