import { Component, OnInit } from '@angular/core';
import * as moment from 'moment-timezone';

import { MenuItem } from 'primeng/api';
import { Product } from '../../api/product';
import { ProductService } from '../../service/productservice';

import { PrimeNGConfig } from 'primeng/api';
import { KetPersonImcService } from '../../services-api/ket-person-imc.service';
import { KetReferoutService } from '../../services-api/ket-referout.service';
import { KetReferbackService } from '../../services-api/ket-referback.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ServicesService } from '../../services-api/services.service';

import { AlertService } from '../../service/alert.service';
import { Subscription } from 'rxjs';
import { AppConfig } from 'src/app/api/appconfig';
import { ConfigService } from 'src/app/service/app.config.service';

@Component({
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

    items: MenuItem[];

    products: Product[];

    chartData: any;

    blockedDocument: boolean = false;


    countOut: any;
    reportOut: any;
    countBack: any;
    reportBack: any;
    countOutReply: any;
    reportOutReply: any;
    countBackReply: any;
    reportBackReply: any;
    sdate: Date;
    edate: Date;
    hcode: any;
    rowsReportData: any = {};


    constructor(
        private productService: ProductService,
        private ketPersonImcService: KetPersonImcService,
        private ketReferoutService: KetReferoutService,
        private ketReferbackService: KetReferbackService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,

        private alertService: AlertService,
        private configService: ConfigService

    ) { 
        // moment().locale('th-TH').format('LT') //=> "13:21"
        this.hcode = sessionStorage.getItem('hcode');
    }

    ngOnInit() {
        this.productService.getProductsSmall().then(data => this.products = data);

        this.items = [
            { label: 'Add New', icon: 'pi pi-fw pi-plus' },
            { label: 'Remove', icon: 'pi pi-fw pi-minus' }
        ];

        this.chartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'First Dataset',
                    data: [65, 59, 80, 81, 56, 55, 40],
                    fill: false,
                    backgroundColor: '#2f4860',
                    borderColor: '#2f4860',
                    tension: .4
                },
                {
                    label: 'Second Dataset',
                    data: [28, 48, 40, 19, 86, 27, 90],
                    fill: false,
                    backgroundColor: '#00bb7e',
                    borderColor: '#00bb7e',
                    tension: .4
                }
            ]
        };

        this.getReport();
    }

    async getReport() {
        this.blockedDocument = true;
        let countOutReport: any;
        let countBackReport: any;
        let countBackReportReply: any;
        let countOutReportReply: any;


        // let sdate = moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
        let sdate ="2021-10-01";
        let edate = moment(this.edate).tz('Asia/Bangkok').format('YYYY-MM-DD')

        console.log(sdate);
        console.log(edate);
        console.log(this.hcode);


        try {
            countBackReport = await this.ketReferbackService.countReport(this.hcode, sdate, edate);
            console.log(countBackReport);
            countBackReportReply = await this.ketReferbackService.countReportReply(this.hcode, sdate, edate);
            console.log(countBackReportReply);


            countOutReport = await this.ketReferoutService.countReport(this.hcode, sdate, edate);
            console.log(countOutReport);
            countOutReportReply = await this.ketReferoutService.countReportReply(this.hcode, sdate, edate);
            console.log(countOutReportReply);

            this.rowsReportData = {
                "countOutReport": countOutReport,
                "countOutReportReply": countOutReportReply,
                "countBackReport": countBackReport,
                "countBackReportReply": countBackReportReply
            }
            console.log(this.rowsReportData);
            this.countOut = this.rowsReportData.countOutReport[0].count;
            this.reportOut = this.rowsReportData.countOutReport[0].report;
            this.countBack = this.rowsReportData.countBackReport[0].count;
            this.reportBack = this.rowsReportData.countBackReport[0].report;

            this.countOutReply = this.rowsReportData.countOutReportReply[0].count;
            this.reportOutReply = this.rowsReportData.countOutReportReply[0].report;
            this.countBackReply = this.rowsReportData.countBackReportReply[0].count;
            this.reportBackReply = this.rowsReportData.countBackReportReply[0].report;
            console.log(this.countOut);
            console.log(this.countBack);
            console.log(this.countOutReply);
            console.log(this.countBackReply);

        } catch (error) {
            console.log(error);
        }



        this.blockedDocument = false;

    }
}
