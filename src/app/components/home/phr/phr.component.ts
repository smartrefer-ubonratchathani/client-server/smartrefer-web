import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';

import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';

import {PhrService} from '../../../services-api/phr.service';
import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-phr',
  templateUrl: './phr.component.html',
  styleUrls: ['./phr.component.css']
})
export class PhrComponent implements OnInit {
  pid:any;
  rowsData:any[] = [];
  rowsDataPhr:any[] = [];
  rowsDataVisit:any[] = [];
  loading: boolean = false;
  loadingPhr: boolean = false;
  loadingVisit: boolean = false;

  selectedRowsData: any = [];
  scrollHeight: string = '';
  validateForm: boolean = false;

  appointment:any[] = [];
  diagnosis:any[] = [];
  drug:any[] = [];
  lab:any[] = [];
  procedure:any[] = [];
  referout:any[] = [];
  screening:any = {};
  itemeCovidVaccine:any[] = [];


  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };


  constructor(
    private phrService: PhrService,
    private alertService:AlertService,

  ) { }

  ngOnInit(): void {
   
  }

  async getPhr(){
    let cid = this.pid;
    this.rowsData = [];
    this.rowsDataPhr = [];

    this.diagnosis = [];
    this.drug = [];
    this.procedure = [];
    this.lab = [];
    this.screening = {};

    this.loading = true;

    try {
      let rs: any = await this.phrService.search(cid);

      if (rs.results) {
        this.rowsData = rs.results;
        this.loading = false;
      } else {
        this.alertService.error('ไม่พบข้อมูล รับบริการ','ข้อมูลผิดพลาด');
        this.loading = false;
      }
      
    } catch (error) {
      console.log(error);
      
    }
  }


  onRowSelect(e: any) {
    let hospcode = e.data.hospcode;
    let hn = e.data.hn;
    let provider = e.data.his_provider;
    this.rowsDataPhr = [];

    this.diagnosis = [];
    this.drug = [];
    this.procedure = [];
    this.lab = [];
    this.screening = {};

    this.getSelectPhr(hospcode,hn,provider)

  }

  async getSelectPhr(hospcode:any,hn:any,provider:any){
    this.loadingPhr = true;

    try {
      let rs: any = await this.phrService.viewsVisit(hospcode,hn,provider);
      if (rs.results) {
        this.rowsDataPhr = rs.results;
        this.loadingPhr = false;
      } else {
        console.log('No Datas');
        // this.alertService.error('ไม่พบข้อมูล รับบริการ','ข้อมูลผิดพลาด');
        this.loadingPhr = false;
      }
      
    } catch (error) {
      console.log(error);
      
    }
  }


  onRowSelectPhr(e: any) {

    let cid = e.data.cid;
    let hospcode = e.data.hospcode;
    let vn = e.data.vn;
    let hn = e.data.hn;
    let provider = e.data.his_provider;
    
    this.diagnosis = [];
    this.drug = [];
    this.procedure = [];
    this.lab = [];
    this.screening = {};

    this.getSelectVisit(cid,hospcode,vn,hn,provider);
    this.getCovidVaccine(cid);

  }

  async getSelectVisit(cid:any,hospcode:any,vn:any,hn:any,provider:any){
    this.loadingVisit = true;

    try {
      let rs: any = await this.phrService.viewsServices(cid,hospcode,vn,hn,provider);
      if (rs) {
        this.diagnosis = rs.diagnosis;
        this.drug = rs.drug;
        this.lab = rs.lab;
        this.procedure = rs.procedure;
        this.screening = rs.screening[0];
      
        this.loadingVisit = false;
      } else {
        console.log('No Datas');
        this.loadingVisit = false;
      }
      
    } catch (error) {
      console.log(error);
      
    }
  }

  async getCovidVaccine(i: any) {
    try {
      let rs: any = await this.phrService.covidvaccine(i);
      if(rs.statusCode == 200){
        this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
      }
    } catch (error) {
      console.log('ไม่พบรายการ');
    }
  }

}
