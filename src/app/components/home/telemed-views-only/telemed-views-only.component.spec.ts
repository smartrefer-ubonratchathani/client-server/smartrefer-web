import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemedViewsOnlyComponent } from './telemed-views-only.component';

describe('TelemedViewsOnlyComponent', () => {
  let component: TelemedViewsOnlyComponent;
  let fixture: ComponentFixture<TelemedViewsOnlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelemedViewsOnlyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TelemedViewsOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
