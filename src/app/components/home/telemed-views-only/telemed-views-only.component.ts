import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild,TemplateRef  } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';
//lookup 
import { ServicesService } from '../../../services-api/services.service';
import { KetTypeptService } from '../../../services-api/ket-typept.service';
import { KetStrengthService } from '../../../services-api/ket-strength.service';
import { KetLoadsService } from '../../../services-api/ket-loads.service';
import { KetThaiaddressService } from '../../../services-api/ket-thaiaddress.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import { KetReferResultService } from '../../../services-api/ket-refer-result.service';
import { KetServiceplanService } from '../../../services-api/ket-serviceplan.service';
import { KetStationService } from '../../../services-api/ket-station.service';
import { async } from '@angular/core/testing';
import { AlertService } from '../../../service/alert.service';
import { Time } from '@angular/common';
import { UntypedFormControl, UntypedFormGroup, Validators,FormControl } from '@angular/forms';


@Component({
  selector: 'app-telemed-views-only',
  templateUrl: './telemed-views-only.component.html',
  styleUrls: ['./telemed-views-only.component.css']
})

export class TelemedViewsOnlyComponent implements OnInit {
  username: any;
  itemStorage: any = [];
  itematt: any = [];

  itemeTypept: any = [];
  itemeStrength: any = [];
  itemeLoads: any = [];
  itemeStation: any = [];
  itemeRefertriage: any = [];
  itemeRefertype: any = [];
  itemeServiceplan: any = [];
  itemeResult: any = [];

  itemeAttachment:any=[];
  loadingAttachment: boolean = true;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];

  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  totime: any = moment(Date()).tz('Asia/Bangkok').format('HH:mm:ss');
  blockedPanel: boolean = false;

  selectedRowsData: any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;
  equipwithpatient:any; 

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;
  todayy: Date = new Date();
  sdate: Date;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout: any;

  allergy: any = [];
  appointment: any = [];
  diagnosis: any = [];
  drugs: any = [];
  hpi: any = {};
  lab: any = [];
  medrecconcile: any = [];
  nurtures: any = [];
  pe: any = {};
  procedure: any = [];
  profile: any = {};
  refer: any = {};
  vaccines: any = [];
  xray: any = [];
  result: any = [];
  serviceplan: any = [];


  referIn: any = {};
  signtext: any = {};

  adddresss: any = {};
  diag_text: any;


  typept_id: any;
  strength_id: any;
  loads_id: any;
  refer_triage_id: any;
  location_refer_id: any;
  refer_type: any;
  expire_date: any;

  refer_result_id: any = '';
  result_id: any = '';
  refer_result_name: any;
  serviceplan_id: any = '';
  receive_spclty_id: any = '';
  receive_ward_id: any;

  bmi: any;
  refer_appoint: any;
  refer_xray_online: any;
  station: any;
  department: any;
  refer_reject_reasons: any;


  textCc: any;
  textPmh: any;
  textPe: any;
  textHpi: any;

  textAddress: any

  refer_sentto_CancerAnywhere:boolean=false;
 
  apooint_telemed: string[] = [];

  selectedResultId: string = '';
  selectedServicePlanId: string = '';

  buttonClose: any;
  station_id:any;
  refer_remark:any;

  itemeCovidVaccine: any = [];
  myTimePicker:any;
 
  @Input() defaultTime: string;
  selectedTime:any;
  timepickerTime: string ="10.20";
  @Input() confirmBtnTmpl: TemplateRef<Node>;
  public date: moment.Moment;
  public formGroup = new UntypedFormGroup({
    date: new UntypedFormControl(null, [Validators.required]),
    date2: new UntypedFormControl(null, [Validators.required])
  })
  public telemed_time = ''
  public maskTime = [ /\d/, /\d/, ':', /\d/, /\d/]
 
  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferoutService: KetReferoutService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
    private alertService:AlertService,
  ) {
    this.sdate = this.todayy;
    this.telemed_time = this.telemed_time;
    this.username = sessionStorage.getItem('username');
    let Typept: any = localStorage.getItem('itemeTypept');
    this.itemeTypept = JSON.parse(Typept);

    let Strength: any = localStorage.getItem('itemeStrength');
    this.itemeStrength = JSON.parse(Strength);

    let Loads: any = localStorage.getItem('itemeLoads');
    this.itemeLoads = JSON.parse(Loads);

    let Station: any = localStorage.getItem('itemeStation');
    this.itemeStation = JSON.parse(Station);

    let Refertriage: any = localStorage.getItem('itemeRefertriage');
    this.itemeRefertriage = JSON.parse(Refertriage);

    let Refertype: any = localStorage.getItem('itemeRefertype');
    this.itemeRefertype = JSON.parse(Refertype);

    let Serviceplan: any = localStorage.getItem('itemeServiceplan');
    this.itemeServiceplan = JSON.parse(Serviceplan);

    let Result: any = localStorage.getItem('itemeResult');
    this.itemeResult = JSON.parse(Result);

  }

  ngOnInit(): void {
    this.lookupDatas();
    let i: any = sessionStorage.getItem('itemStorage');  
    this.itemStorage = JSON.parse(i);

      this.getReferIn(this.itemStorage);

  }


  
pCheckTelemed(e: any) {

  if(this.apooint_telemed.length == 0){
    this.apooint_telemed = ['0'];
  }else{
    this.apooint_telemed = ['1'];
  }

}

  // get datas go to pageupload
    uploadsRoute(){
      let datas = this.itemStorage;
      let strdata:any = JSON.stringify(datas);
      sessionStorage.setItem('strdata',strdata);
      this.router.navigate(['/home/uploads']);
  }
  onTimeChange(e){
    this.telemed_time = e;
  }
  onDateSelected() {
    // console.log('on date select');
   
  }
  async getReferIn(i: any) {

    try {
      let rs: any = await this.ketReferoutService.receive(i.refer_no,);
      if (rs) {
        this.allergy = rs.allergy
        this.appointment = rs.appointment
        this.diagnosis = rs.diagnosis
        this.drugs = rs.drugs
        this.lab = rs.lab
        this.medrecconcile = rs.medrecconcile
        this.signtext = rs.signtext[0]
        this.procedure = rs.procedure
        this.xray = rs.xray
        this.referIn = rs.referout
        this.refer_result_id = this.referIn.receive_refer_result_id || '';
        this.serviceplan_id = this.referIn.receive_serviceplan_id || '';
        this.receive_spclty_id = this.referIn.receive_spclty_id || '';
        this.receive_ward_id = this.referIn.receive_ward_id || '';
        this.refer_reject_reasons = this.referIn.reject_refer_reason;

        this.station_id = this.referIn.location_refer_id;
        this.typept_id = this.referIn.typept_id
        this.strength_id = this.referIn.strength_id
        this.loads_id = this.referIn.loads_id
        this.refer_triage_id = +this.referIn.refer_triage_id;
        this.refer_type = this.referIn.refer_type;
        this.refer_appoint = +this.referIn.refer_appoint;
        this.refer_xray_online = +this.referIn.refer_xray_online;
        this.equipwithpatient = this.referIn.equipwithpatient;
        this.refer_remark = this.referIn.refer_remark;
        this.expire_date = this.referIn.expire_date;
        this.sdate = this.referIn.tele_med_date;
        this.telemed_time = this.referIn.tele_med_time;
        this.apooint_telemed=this.referIn.apooint_telemed[0];
        this.refer_sentto_CancerAnywhere=this.referIn.refer_sentto_CancerAnywhere;

        this.buttonClose = this.referIn.receive_spclty_id;

        this.diag_text = this.nurtures.diag_text;

        this.textCc = this.nurtures.cc;
        this.textPmh = this.nurtures.pmh;
        if (this.pe) {
          this.textPe = this.pe.pe;
        }
        if (this.hpi) {
          this.textHpi = this.hpi.hpi;
        }

        if (this.nurtures.weight && this.nurtures.height) {

          this.bmi = (this.nurtures.weight / ((this.nurtures.height / 100) * (this.nurtures.height / 100)));
        }


        if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
          if (this.profile.moopart && this.profile.moopart != "" && this.profile.moopart != "00") {
            this.getAddress_full(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart, this.profile.moopart);
          } else {
            this.getAddress(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart);
          }
        }

        this.getAttachment(this.referIn.refer_no);

        //ปิดระบบ
        // this.getCovidVaccine(this.referIn.cid);
        // console.log(this.referIn.refer_no);

      } else {


      }

    } catch (error) {
      console.log(error);
      this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');

    }
  }
  lookupDatas(){
    this.getService();
  }
  
  async getService(){
    try {
      let rs:any = await this.servicesService.department();

      this.department = rs;
    } catch (error) {
      console.log('error',error);
      
    }
  }
  async getAddress_full(chwpart: any, amppart: any, tmbpart: any, moopart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select_full(chwpart, amppart, tmbpart, moopart);

      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error',error);
    }
  }

  async getAddress(chwpart: any, amppart: any, tmbpart: any) {
    try {
      let rs: any = await this.ketThaiaddressService.select(chwpart, amppart, tmbpart);

      this.textAddress = rs[0].full_name
    } catch (error) {
      console.log('error',error);
    }
  }

  async onSaveChange(){
    if(this.refer_result_id && this.receive_spclty_id && this.serviceplan_id){
      this.onSave();
    }else if(this.refer_result_id == '2'){
      this.onSave();
    }else{
      this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาเลือกข้อมูลใหม่');
    }
  }

  async onSave() {
    let result_name: any;
    this.itemeResult.forEach((e: any) => {
      if (e.refer_result_id == this.refer_result_id) {
        result_name = e.refer_result_name
      }
    });

    let receive_spclty: any;
    this.itemeStation.forEach((e: any) => {
      if (e.station_id == this.receive_spclty_id) {
        receive_spclty = e.station_name
      }
    });

    let serviceplan_name: any;
    this.itemeServiceplan.forEach((e: any) => {
      if (e.serviceplan_id == this.serviceplan_id) {
        serviceplan_name = e.serviceplan_name
      }
    });

    let ward_name: any;
    this.department.forEach((e: any) => {
      if (e.dep_code == this.receive_ward_id) {
        ward_name = e.dep_name
      }
    });


    let strength_name: any;
    this.itemeStrength.forEach((e: any) => {
      if (e.strength_id == this.strength_id) {
        strength_name = e.strength_name
      }
    });
    let telemeddate =  moment(this.sdate).tz('Asia/Bangkok').format('YYYY-MM-DD');

    var dataPost = {
      "referout": {
        "receive_no": this.referIn.refer_no,
        "receive_date": this.today,
        "receive_time": this.totime,
        "receive_station_id": "",
        "receive_station_name": "",
        "receive_spclty_id": this.receive_spclty_id,
        "receive_spclty_name": receive_spclty,
        "receive_refer_result_id": this.refer_result_id,
        "receive_refer_result_name": result_name,
        "receive_serviceplan_id": this.serviceplan_id,
        "receive_serviceplan_name": serviceplan_name,
        "reject_refer_reason": this.refer_reject_reasons,
        "receive_ward_id": this.receive_ward_id,
        "receive_ward_name": ward_name,
        "providerUser": this.username,
        "strength_id": this.strength_id,
        "strength_name": strength_name,
        "tele_med_date" : telemeddate,
        "tele_med_time" : this.telemed_time+':00'
      }
    }

    let info: any = {

    }
    info.rows = dataPost;

    try {
      let rs: any = await this.ketReferoutService.onUpdate(info, this.referIn.refer_no)

      if(rs.res_.ok == true){
          this.alertService.success('บันทึกสำเร็จเรียบร้อย','ดำเนินการเสร็จเรียบร้อย');
          this.router.navigate(['/home/telemed'])
        }else{
          console.log('error',rs.res_.error);
          this.alertService.error(rs.res_.error.salMessage,'บันทึกสำเร็จไม่สำเร็จ');
        }
        } catch (error) {
      console.log(error);
      this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');

  
    }
  }

  onClickAssess() {
    this.router.navigate(['/home/assess'])
  }

  async onPrintReferIn() {
    this.router.navigate(['/home/printreferout']);
  }

async getAttachment(i:any) {
  this.loadingAttachment = true;
  try {
    let rs: any = await this.ketAttachmentService.select(i);

    if (rs[0]) {
      this.itemeAttachment = rs;
      this.loadingAttachment = false;
    } else {
      this.loadingAttachment = false;
    }
  } catch (error) {
    console.log(error);
    this.loadingAttachment = false;
  }
  
}

async downloadAttRoute(filename: any){

  let download: any = await this.ketAttachmentService.download(filename);
}
onPropertyChange(city) {
  return city.viweModel;    
}
async getCovidVaccine(i: any) {
  try {
    let rs: any = await this.ketReferoutService.covidvaccine(i);

    if(rs.statusCode == 200){
      this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
    }

  } catch (error) {
    console.log('ไม่พบรายการ');
  }
}
}
