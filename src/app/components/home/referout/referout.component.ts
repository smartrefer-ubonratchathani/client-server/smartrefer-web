import * as moment from 'moment-timezone';
import {
    Component,
    OnInit,
    Input,
    ElementRef,
    NgZone,
    ViewChild,
    
} from '@angular/core';

import { Router, NavigationExtras } from '@angular/router';
import { slideInOutAnimation } from '../../../animations/index';
import { Table } from 'primeng/table';
import { KetReferoutService } from '../../../services-api/ket-referout.service';
import { GlobalVariablesService } from '../../../shared/globalVariables.service';
import { ServicesService } from '../../../services-api/services.service';

import { AlertService } from '../../../service/alert.service';
import { KetAttachmentService } from '../../../services-api/ket-attachment.service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
// import { InitTableServcie } from '../../../service/inittable.service';

import { MqttClient } from 'mqtt';
import * as mqttClient from '../../../vendor/mqtt';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { log } from 'console';

@Component({
    selector: 'app-referout',

    templateUrl: './referout.component.html',
    styleUrls: ['./referout.component.css'],
    animations: [slideInOutAnimation],
    host: { '[@slideInOutAnimation]': '' },
})
export class ReferoutComponent implements OnInit {
    @ViewChild('pdfTable') pdfTable!: ElementRef;

    isOffline = false;
    client: MqttClient;
    notifyUser = null;
    notifyPassword = null;
    notifyUrl: string;

    start_out_date: any;
    end_out_date: any;

    scrHeight: number = 0;
    scrWidth: number = 0;
    boxHeight: any;
    rowsData: any[] = [];
    device: any = 't';

    // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
    today: Date = new Date();

    blockedPanel: boolean = false; //สำหรับ ป้องกันหน้าจอตอนโปรเซสทำงานยังไม่เสร็จ

    selectedRowsData: any = [];

    statuses: any[] = [];

    loading: boolean = true;

    displayCreate: boolean = false;
    rowsDataCreate: any[] = [];
    loadingCreate: boolean = true;
    rowsDataTemp: any[] = [];
    rowsReportData: any = {};
    displayReferOut: boolean = false;
    rowsDataReferOut: any[] = [];
    loadingReferOut: boolean = true;

    @ViewChild('dt') table!: Table;

    @ViewChild('dtCreate') tableCreate!: Table;

    first: any = 0;

    rows: any = 10;

    hcode: any;

    sdate: Date;
    edate: Date;

    sdateCreate: any;
    edateCreate: any;

    limitReferOut: any = 2000;

    buttonClassStrength: string = 'p-button-rounded p-button-warning';
    value3: any;
    justifyOptions: any = [];
    value1: string = 'off';
    stateOptions: any = [];

    ptableStyle: any = {
        width: '100%',
        // height: '100%',
        flex: '1 1 auto',
    };
    scrollHeight: string = '';

    validateForm: boolean = false;

    checked: boolean = true;
    activeIndex1: number = 0;
    cid: any;

    exportColumns: any = [];
    paginator: boolean = true;
    colHnWidth: any = '0 0 80px';
    specTable: any;
    visible: boolean;
    refer_no: any;

    referout_i: any;

    rowsData1: any = [];
    rowsData2: any = [];
    strChecked1: string = 'แสดงเฉพาะรายการขอนัด';
    checked1: boolean = false;

    constructor(
        // private primengConfig: PrimeNGConfig,
        private ketReferoutService: KetReferoutService,
        private globalVariablesService: GlobalVariablesService,
        private servicesService: ServicesService,
        private router: Router,
        private alertService: AlertService,
        private ketAttachmentService: KetAttachmentService,
        // private initTableServcie: InitTableServcie,
        private zone: NgZone,
        private http: HttpClient,
       
    ) {

        this.hcode = sessionStorage.getItem('hcode');
        // this.sdate = new Date('2560-12-10');
        this.sdate = this.today;
        this.edate = this.today;

        this.sdateCreate = this.today;
        this.edateCreate = this.today;
        this.cid = this.cid;


        // config mqtt
        this.notifyUrl = `ws://203.113.117.66:8080`;
        this.notifyUser = `q4u`;
        this.notifyPassword = `##q4u##`;
    }
    // Medthod  ที่ต้องเหมือนกันทุก component/////////////////////
    readGlobalValue() {
        this.scrHeight = Number(this.globalVariablesService.scrHeight);
        this.scrWidth = Number(this.globalVariablesService.scrWidth);
        this.boxHeight = this.scrHeight - 80 + 'px';
        // console.log(this.scrHeight + ":" + this.scrWidth);
        this.scrollHeight =
            Number(this.globalVariablesService.scrHeight) - 380 + 'px';
        // กำหนด ความสูง  ความกว้างของตาราง
        this.ptableStyle = {
            // width: (this.scrWidth-20) + 'px',
            width: '100%',
            height: this.scrHeight - 300 + 'px',
        };
    }
    // captureScreen() {
    //   let data = document.getElementById('dt');
    //   html2canvas(data as any).then(canvas => {
    //       var imgWidth = 210;
    //       var pageHeight = 295;
    //       var imgHeight = canvas.height * imgWidth / canvas.width;
    //       var heightLeft = imgHeight;
    //       const contentDataURL = canvas.toDataURL('image/png');
    //       let pdfData = new jsPDF('p', 'mm', 'a4');
    //       var position = 0;
    //       pdfData.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
    //       pdfData.save(`MyPdf.pdf`);
    //   });
    // }

    // exportPdf1() {
    //   import("jspdf").then(jsPDF => {
    //       import("jspdf-autotable").then(x => {
    //           const doc = new jsPDF.default('p', 'mm', 'a4');
    //           autoTable(doc, { html: '#dt' })
    //           doc.save('products.pdf');
    //       })
    //   })
    // }

    // exportPdf() {

    //   var source = document.getElementById('tabl');

    //           // const doc = new jsPDF('p', 'mm', 'a4');
    //           const doc = new jsPDF('p', 'pt');
    //           autoTable(doc, { html: '#tabl' })
    //           doc.save('products.pdf');

    // }

    // กำหนดค่าเกี่ยวกับ paginator  table
    reset() {
        this.first = 0;
    }
    isLastPage(): boolean {
        return this.rowsData
            ? this.first === this.rowsData.length - this.rows
            : true;
    }
    isFirstPage(): boolean {
        let r = this.rowsData ? this.first === 0 : true;
        return this.rowsData ? this.first === 0 : true;
    }
    next() {
        this.first = this.first + this.rows;
    }
    prev() {
        this.first = this.first - this.rows;
    }
    ////////////////////////////////////////

    ngOnInit() {
        this.readGlobalValue();
        this.getInfo();
        this.connectWebSocket();
        // this.primengConfig.ripple = true;
        if (
            sessionStorage.getItem('start_out_date') &&
            sessionStorage.getItem('end_out_date')
        ) {
            this.sdate = JSON.parse(sessionStorage.getItem('start_out_date'));
            this.edate = JSON.parse(sessionStorage.getItem('end_out_date'));
        }
    }

    onSearch() {
        this.getInfo();
    }

    onDateSelect(value: any) {
        this.table.filter(this.formatDate(value), 'date', 'equals');
    }

    formatDate(date: any) {
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = '0' + month;
        }

        if (day < 10) {
            day = '0' + day;
        }

        return date.getFullYear() + '-' + month + '-' + day;
    }

    ///////dialog ต่ออายุใบรีเฟอร์
    showDialog(i) {
        this.referout_i = i;
        this.visible = true;
        this.refer_no = i;
    }

    async getInfo() {
        this.loading = true;
        this.rowsData = [];
        this.rowsData1 = [];
        this.rowsData2 = [];

        let startDate: any;
        let endDate: any;

        this.start_out_date = moment(
            JSON.parse(sessionStorage.getItem('start_out_date'))
        ).format('YYYY-MM-DD');
        this.end_out_date = moment(
            JSON.parse(sessionStorage.getItem('end_out_date'))
        ).format('YYYY-MM-DD');

        if (
            sessionStorage.getItem('start_out_date') &&
            sessionStorage.getItem('end_out_date')
        ) {
            startDate = this.start_out_date;
            endDate = this.end_out_date;
        } else {
            startDate =
                this.sdate.getFullYear() +
                '-' +
                (this.sdate.getMonth() + 1) +
                '-' +
                this.sdate.getDate();
            endDate =
                this.edate.getFullYear() +
                '-' +
                (this.edate.getMonth() + 1) +
                '-' +
                this.edate.getDate();
        }

        let rs: any;
        try {
            if (this.cid) {
                rs = await this.ketReferoutService.selectCid(
                    this.cid,
                    this.hcode,
                    'OUT'
                );
            } else {
                console.log('startDate ',startDate);
                console.log('endDate ',endDate);
                
                rs = await this.ketReferoutService.select(
                    this.hcode,
                    startDate,
                    endDate,
                    this.limitReferOut
                );
            }
            // เฉพาะรายงานขอนัด
            rs.forEach((v: any) => {
                if(v.receive_spclty_name == 'กรุณาเลือก'){
                    v.receive_spclty_name = '';
                }

                if (
                    v.refer_appoint == '1' ||
                    v.refer_appoint == '2' ||
                    v.refer_appoint == '4'
                ) {
                    this.rowsData1.push(v);
                }
               
            });

            // รายงานทั้งหมด
            rs.forEach((v: any) => {
                if(v.receive_spclty_name == 'กรุณาเลือก'){
                    v.receive_spclty_name = '';
                }   

                if (
                    v.refer_appoint == '0' ||
                    v.refer_appoint == '1' ||
                    v.refer_appoint == '2' ||
                    v.refer_appoint == '3' ||
                    v.refer_appoint == '4'
                ) {
                    this.rowsData2.push(v);
                }
               
            });

            this.loading = false;
            this.onCheckedAppoint(this.checked1);
 
            // let item: any = rs[0];

            // if (item) {
            //     this.rowsData = await rs;
            //     this.loading = false;

            // } else {

            //     this.loading = false;
            // }
        } catch (error) {
            console.log(error);
            this.loading = false;
        }
    }

    async onCreate() {

        this.globalVariablesService.paramsChild = 'paramFrom ReferOut';
        let navigationExtras: NavigationExtras = {
            queryParams: {
                firstname: 'Nic',
                lastname: 'Raboy',
            },
        };

        this.router.navigate(['/home/referout-create']);

        sessionStorage.setItem('routmain', '/home/referout');

        // this.router.navigate(['/product-list'], { queryParams: { page: "pageNum" } });
    }

    async getCreateReferOut() {

        this.loadingCreate = true;
        try {
            let rs: any = await this.servicesService.referout(
                this.sdateCreate,
                this.edateCreate
            );
            let item: any = rs[0];
            if (item) {
                this.rowsDataCreate = rs;
                this.loadingCreate = false;
            } else {
                this.loadingCreate = false;
            }
        } catch (error) {
            console.log(error);
            this.loadingCreate = false;
        }
    }

    async onSearchCreate() {

        this.justifyOptions = [
            { icon: 'pi pi-align-left', justify: 'Left' },
            { icon: 'pi pi-align-right', justify: 'Right' },
            { icon: 'pi pi-align-center', justify: 'Center' },
            { icon: 'pi pi-align-justify', justify: 'Justify' },
        ];
        this.stateOptions = [
            { label: 'Off', value: 'off' },
            { label: 'On', value: 'on' },
        ];
        this.getCreateReferOut();
    }
    async onReferOut(i: any) {
        this.getReferOut(i);
        this.displayReferOut = true;
        this.displayCreate = false;
    }

    async getReferOut(i: any) {
        this.loadingReferOut = true;
        try {
            let rs: any = await this.servicesService.view(
                i.hn,
                i.seq,
                i.referno
            );

            let item: any = rs[0];
            if (item) {

                this.rowsDataReferOut = rs;
                this.loadingReferOut = false;
            } else {
                // console.log();
            }
        } catch (error) {
            console.log(error);
            this.loadingReferOut = false;
        }
    }
    onDateSelected() {
        // console.log('lddl');
        if (this.edate < this.sdate) {
            this.alertService.error(
                'ข้อผิดพลาด !',
                'วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้'
            );
            this.validateForm = false;
        } else {
            this.validateForm = true;
        }
    }

    onRowSelect(event: any) {

        sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
        this.router.navigate(['/home/referout-views-only']);
    }

    async onReferOutView(i: never) {
        sessionStorage.setItem('itemStorage', JSON.stringify(i));
        this.router.navigate(['/home/referout-views-only']);
        sessionStorage.setItem('routmain', '/home/referout');
    }

    handleChange(e: any) {
        let isChecked = e.checked;
    }

    uploadsRoute(datas: any) {

        let strdata: any = JSON.stringify(datas);
        sessionStorage.setItem('strdata', strdata);
        this.goToLink('/home/uploads');
    }

    referOutViewsRoute() {

        this.router.navigate(['/home/referout-views']);
        sessionStorage.setItem('routmain', '/home/referout');
    }
    async deleteReferOut(i: any) {

        let confirm = await this.alertService.confirm(
            'ต้องการลบรายการนี้ ใช่หรือไม่'
        );

        if (confirm) {
            let rs = await this.ketReferoutService.onDelete(i);
            this.getInfo();
        }
    }
    navigatNewWindow(router: Router, commands: any[]) {
        let baseUrl = window.location.href.replace(router.url, '');
        const url = new URL(commands.join('/'), baseUrl).href;
        window.open(url, '_blank');
    }
    goToLink(link: any) {

        const url = this.globalVariablesService.urlSite+link
        window.open(url, '_blank');
    }

    async onRenewReferToday() {

        let dataPost: any = {
            rows: {
                refer_no: this.refer_no,
                expire_date: moment(new Date().getTime())
                    .tz('Asia/Bangkok')
                    .format('YYYY-MM-DD'),
            },
        };
        let rs = await this.ketReferoutService.onRenewRefer(dataPost);

        this.alertService.success(
            'วันหมดอายุขยายถึง  วันที่ ' +
                moment(new Date().getTime())
                    .tz('Asia/Bangkok')
                    .format('YYYY-MM-DD')
        );
        this.getInfo();
        this.visible = false;
    }

    async onRenewRefer1Year() {

        let dataPost: any = {
            rows: {
                refer_no: this.refer_no,
                expire_date: moment(new Date().getTime() + 365 * 86400e3)
                    .tz('Asia/Bangkok')
                    .format('YYYY-MM-DD'),
            },
        };
        let rs = await this.ketReferoutService.onRenewRefer(dataPost);
        this.alertService.success(
            'วันหมดอายุขยายถึง  วันที่ ' +
                moment(new Date().getTime() + 365 * 86400e3)
                    .tz('Asia/Bangkok')
                    .format('YYYY-MM-DD')
        );
        this.getInfo();
        this.visible = false;
    }

    async downloadAttRoute(filename: any) {
        let download: any = await this.ketAttachmentService.download(filename);
    }

    onClickSssess(datas: any) {
        let Storage: any = JSON.stringify(datas);
        sessionStorage.setItem('itemStorage', Storage);
        this.router.navigate(['/home/assess-view']);
    }

    exportExcel() {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(this.rowsData);
            const workbook = {
                Sheets: { data: worksheet },
                SheetNames: ['data'],
            };
            const excelBuffer: any = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array',
            });
            this.saveAsExcelFile(excelBuffer, 'referout');
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE =
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE,
        });
        FileSaver.saveAs(
            data,
            fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
        );
    }

    async exportexcel2() {
        // this.paginator = false;
        // this.rowsDataTemp = this.rowsData;
        // this.rowsData = [];
        // this.rowsData = this.reDraw();
        let checkT = await this.reDraw();
        if (checkT) {
            let fileName = 'ExcelSheet.xlsx';
            /* table id is passed over here */
            let element = document.getElementById('dt');
            const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

            /* generate workbook and add the worksheet */
            const wb: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

            /* save to file */
            XLSX.writeFile(wb, fileName);
        }
    }

    async reDraw() {
        this.paginator = false;
        this.rowsDataTemp = this.rowsData;
        this.rowsData = [];
        this.rowsData = this.rowsDataTemp;
        return true;
    }
    async exportExcel3() {
        // this.rowsDataTemp = this.rowsData;
        let fileName = 'ExcelSheet.xlsx';
        /* table id is passed over here */
        let element = document.getElementById('dtTemp');
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, fileName);
    }

    valuechange(event: any) {
        if (event.target.value.length == 13) {
            this.validateForm = true;
        }
    }

    async onRegisterRefer(referid) {}

    connectWebSocket() {
        // const rnd = new Random();
        const clientId = `smartrefer-${new Date().getTime()}`;

        try {
            this.client = mqttClient.connect(this.notifyUrl, {
                clientId: clientId,
                username: this.notifyUser,
                password: this.notifyPassword,
            });
        } catch (error) {
            console.log(error);
        }

        const topic = `smartrefer/${this.hcode}`;

        const that = this;

        this.client.on('message', async (topic, payload) => {
            try {
                const _payload = JSON.parse(payload.toString());
                if (_payload.refer_no) {
                    //load datas
                    this.getInfo();
                } else {
                    // this.clearData();
                }
            } catch (error) {
                console.log(error);
            }
        });

        this.client.on('connect', () => {
            // console.log(`Connected!`);
            that.zone.run(() => {
                that.isOffline = false;
            });

            that.client.subscribe(topic, { qos: 0 }, (error) => {
                if (error) {
                    that.zone.run(() => {
                        that.isOffline = true;
                        try {
                            // that.counter.restart();
                        } catch (error) {
                            console.log(error);
                        }
                    });
                } else {
                    // console.log(`subscribe ${topic}`);
                }
            });
        });

        this.client.on('close', () => {
            // console.log('MQTT Conection Close');
        });

        this.client.on('error', (error) => {
            // console.log('MQTT Error');
            that.zone.run(() => {
                that.isOffline = true;
                // that.counter.restart();
            });
        });

        this.client.on('offline', () => {
            // console.log('MQTT Offline');
            that.zone.run(() => {
                that.isOffline = true;
                try {
                    // that.counter.restart();
                } catch (error) {
                    console.log(error);
                }
            });
        });
    }

    onCheckedAppoint(e: any) {

        let isChecked = e;

        if (isChecked) {
            this.rowsData = this.rowsData1;
            // this.strChecked1 = ' แสดงมีนัดแล้ว';
            
        } else {
            this.rowsData = this.rowsData2;
            // this.strChecked1 = ' ปิดมีนัดแล้ว';

        }
        // console.log(this.rowsData);
    }
}
