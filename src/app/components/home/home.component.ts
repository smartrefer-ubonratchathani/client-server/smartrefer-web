import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy, PopStateEvent } from '@angular/common';
// import 'rxjs/add/operator/filter';

import { Router, NavigationEnd, NavigationStart } from '@angular/router';
// import {NavbarComponent} from '../layout-components/navbar/navbar.component'
import {GlobalVariablesService} from '../../shared/globalVariables.service';
import { BreadcrumbService } from 'xng-breadcrumb';

//lookup 
// import { KetTypeptService } from '../../services-api/ket-typept.service';
// import { KetStrengthService } from '../../services-api/ket-strength.service';
// import { KetLoadsService } from '../../services-api/ket-loads.service';
// import {KetReferResultService} from '../../services-api/ket-refer-result.service';
// import {KetServiceplanService} from '../../services-api/ket-serviceplan.service';
// import {KetStationService} from '../../services-api/ket-station.service';
// import {VersionService} from '../../services-api/version.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  scrHeight: any = 0;
  scrWidth: number = 0;
  boxHeight: any ;
  boxHeight1: any ='1348px';

  screenStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };

  itemeTypept:any=[];
  itemeStrength:any=[];
  itemeLoads:any=[];
  itemeResult:any=[];
  itemeServiceplan:any=[];
  itemeStation:any=[];
  itemeRefertriage:any=[];

  colHnWidthhome: any = '0 0 80px';

  constructor(
    public location: Location, 
    private router: Router,
    private globalVariablesService:GlobalVariablesService,
    private breadcrumbService: BreadcrumbService,
    ) {
     
     }

    readGlobalValue() {
      this.scrHeight = Number(this.globalVariablesService.scrHeight);
      this.scrWidth = Number(this.globalVariablesService.scrWidth);
      this.boxHeight = ((this.scrHeight)-100)+'px';

      this.screenStyle = {
        width: '500px',
        height: (this.scrHeight-200)+'px'
      }
      // do something with the value read out
    }
  ngOnInit() {
    this.readGlobalValue();

    let token = sessionStorage.getItem('token');
    if (!token) {
      this.router.navigate(['/login']);
    }

  }


}
