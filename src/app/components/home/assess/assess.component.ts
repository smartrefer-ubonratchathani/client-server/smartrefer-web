
import * as moment from 'moment-timezone';
import { Component, OnInit,Input, ViewChild} from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import {slideInOutAnimation,fadeInAnimation} from 'src/app/animations/index';
// import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { KetEvaluateService } from '../../../services-api/ket-evaluate.service';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { KetReferoutService } from '../../../services-api/ket-referout.service';

// import { KetEvaluateService } from '../../../services-api/ket-evaluate.service
import { AlertService } from '../../../service/alert.service';

@Component({
  selector: 'app-assess',
  templateUrl: './assess.component.html',
  styleUrls: ['./assess.component.css']
})
export class AssessComponent implements OnInit {
  loading: boolean = true;
  refer_no: string ='';
  modalTaken = 'true';

  rowsData: any[] = [
    {'field':1},

  ];

  loadingReferOut: boolean = true;

  rowsDataReferOut:any={};

  refer_his_no: string='';
  refer_date: any;
  hn: string='';
  fullname: string='';
  age: string='';
  pid: string='';
  an: string='';
  cid: string='';
  sex: string='';
  occupation: string='';
  

  answer_1: any = '1';
  answer_1_remark: any = '';
  answer_2: any = '1';
  answer_2_remark: any = '';
  answer_3: any = '1';
  answer_3_remark: any = '';
  answer_4: any = '1';
  answer_4_remark: any = '';
  answer_5: any = '1';
  answer_5_remark: any = '';
  other_remark: any = '';
  username: any;
  hcode: any;
  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private router: Router,
    private ketEvaluateService: KetEvaluateService,
    private ketReferoutService: KetReferoutService,
    private alertService: AlertService,

  ) {
    this.username = sessionStorage.getItem('username');
    this.hcode = sessionStorage.getItem('hcode');
   }

  ngOnInit() {
    this.getInfo();

  }
  async getInfo() {
    let items: any=sessionStorage.getItem('itemStorage');
    let i:any =JSON.parse(items);
    this.loading = true;
    this.refer_no = i.refer_no;
    this.getReferOut(this.refer_no); //ให้ทำงานหลัง refer_no ได้ค่ามาครับ

    try {
      let rs: any = await this.ketEvaluateService.select_eva_detail(i.refer_no);

      let item: any = rs[0];

      if (item) {
        this.rowsData = rs;
        this.loading = false;
      } else {
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      this.loading = false;
    }
    
  }
  async getReferOut(i: any) {

    this.loadingReferOut= true;
    try {
      let rs: any = await this.ketReferoutService.receive(i); // ดังนี้ i = refer_no
      let item: any = rs.referout;  // รับค่า rs มาส่ง referout เข้า item
      
      if (item) { // เอาค่าลง ตัวแปร
          this.refer_his_no=item.refer_his_no 
          this.refer_date =item.refer_date
          this.hn=item.hn
          this.fullname=`${item.pname}${item.fname} ${item.lname} `; //ประกรอบชื่อ สกุล
          this.age=item.age
          this.pid=item.pid
          this.an=item.an
          this.cid=item.cid
          this.sex=item.sex
          this.occupation=item.occupation
      }
      else{
        this.loadingReferOut= false;
      }
    } catch (error) {
      console.log(error);

    }
  }

  onChangeSave(){
    if(this.refer_no && this.answer_1 && this.answer_2 && this.answer_3 && this.answer_4 && this.answer_5){
      this.onSave();
    }else{
      this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาเลือกข้อมูลใหม่');
    }
  }

  async onSave(){
    let info:any = {
      "refer_no":this.refer_no,
      "answer_1":this.answer_1,
      "answer_1_remark":this.answer_1_remark,
      "answer_2":this.answer_2,
      "answer_2_remark":this.answer_2_remark,
      "answer_3":this.answer_3,
      "answer_3_remark":this.answer_3_remark,
      "answer_4":this.answer_4,
      "answer_4_remark":this.answer_4_remark,
      "answer_5":this.answer_5,
      "answer_5_remark":this.answer_5_remark,
      "other_remark":this.other_remark,
      "create_date":this.today,
      "create_by":this.username
    }
    let datas:any = {};
    datas.rows = JSON.stringify(info);

    try {
      let rs:any = await this.ketEvaluateService.onSave(datas);
      this.alertService.success('บันทึกสำเร็จ');
      this.router.navigate(['/home/referin'])
    } catch (error) {
      console.log(error);
      
    }
  }
}
