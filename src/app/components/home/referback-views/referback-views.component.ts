import * as moment from 'moment-timezone';
import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';


import { Table } from 'primeng/table';
import { PrimeNGConfig } from 'primeng/api';
import { GlobalVariablesService } from 'src/app/shared/globalVariables.service';
import { ThisReceiver } from '@angular/compiler';


//lookup 
import { ServicesService } from '../../../services-api/services.service';
import {KetTypeptService} from '../../../services-api/ket-typept.service';
import {KetStrengthService} from '../../../services-api/ket-strength.service';
import {KetLoadsService} from '../../../services-api/ket-loads.service';
import {KetThaiaddressService} from '../../../services-api/ket-thaiaddress.service';
import {KetReferbackService} from '../../../services-api/ket-referback.service';
import {KetAttachmentService} from '../../../services-api/ket-attachment.service';
import {KetReferResultService} from '../../../services-api/ket-refer-result.service';
import {KetServiceplanService} from '../../../services-api/ket-serviceplan.service';
import {KetStationService} from '../../../services-api/ket-station.service';
// import { async } from '@angular/core/testing';
import { AlertService } from '../../../service/alert.service';
import { AnywhereComponent } from '../anywhere/anywhere.component';



@Component({
  selector: 'app-referback-views',
  templateUrl: './referback-views.component.html',
  styleUrls: ['./referback-views.component.css']
})
export class ReferbackViewsComponent implements OnInit {
  itemStorage: any = [];
  itemeStation: any = [];
  itemeResult: any = [];
  itemeTypes:any=[]
  itematt:any=[];

  is_coc:any=0;

  itemeTypept:any=[];
  itemeStrength:any=[];
  itemeLoads:any=[];
  itemeReferCause:any=[];
  itemeRefertype:any=[];
  itemeAttachment:any=[];
  loadingAttachment: boolean = true;

  username: any;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  
  today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  totime: any = moment(Date()).tz('Asia/Bangkok').format('HH:mm:ss');

  blockedPanel: boolean = false;

  collapsed:boolean=true;

  selectedRowsData:any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;

  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  @ViewChild('dt') table!: Table;

  @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: any;
  edate: any;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 2000;


  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';
  itemreferout:any;

  allergy:any=[];
  appointment:any=[];
  diagnosis:any=[];
  drugs:any=[];
  hpi:any={};
  lab:any=[];
  medrecconcile:any=[];
  nurtures:any=[];
  pe:any={};
  procedure:any=[];
  profile:any={};
  refer:any={};
  vaccines:any=[];
  xray:any=[];
  result:any=[];
  serviceplan:any=[];
  receive_ward_id:any=[];

  referback:any={};
  signtext:any={};

  adddresss:any={};
  diag_text:any;

  cause_referback_id:any = "";
  typept_id:any = "";
  station_id:any = "";
  strength_id:any = "";
  loads_id:any;
  refer_triage_id:any;
  location_refer_id:any;
  refer_type:any = "";
  expire_date:any;

  refer_result_id:any;
  serviceplan_id:any;
  receive_spclty_id:any;
  refer_remark:any
  equipwithpatient:any;
  
  bmi:any;
  refer_appoint: any = 0;
  refer_xray_online:any;
  station:any;
  department:any;


  textCc:any;
  textPmh:any;
  textPe:any;
  textHpi:any;

  textAddress:any
  expander:boolean=true;
  itemeCovidVaccine: any = [];
  blockedDocument: boolean = false;



  constructor(
    private primengConfig: PrimeNGConfig,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private ketTypeptService: KetTypeptService,
    private ketStrengthService: KetStrengthService,
    private ketLoadsService: KetLoadsService,
    private ketThaiaddressService: KetThaiaddressService,
    private ketReferbackService: KetReferbackService,
    private ketAttachmentService: KetAttachmentService,
    private ketReferResultService: KetReferResultService,
    private ketServiceplanService: KetServiceplanService,
    private ketStationService: KetStationService,
    private alertService:AlertService,

  ) { 
    this.username = sessionStorage.getItem('username');
    this.hcode = sessionStorage.getItem('hcode');

    this.itemeTypes = [{
      refer_type_id:"",
      refer_type_name:"",

    }]
  }

  ngOnInit(): void {

    
    // this.lookupDatas();
    let i: any = sessionStorage.getItem('itemStorage');
        this.itemStorage = JSON.parse(i);

        let Result:any = localStorage.getItem('itemeResult');
        this.itemeResult = JSON.parse(Result);

        let Loads:any = localStorage.getItem('itemeLoads');
        this.itemeLoads = JSON.parse(Loads);
    
        let ReferCause:any = localStorage.getItem('itemeReferCause');
        this.itemeReferCause = JSON.parse(ReferCause);

        let Refertype:any = localStorage.getItem('itemeRefertype');
        this.itemeRefertype = JSON.parse(Refertype);

        if (!this.itemStorage) {
            this.router.navigate(['/home/referback-views']);
        } else {
            this.getReferBack(this.itemStorage);
        }
  }

  async getStation(hospcode:any) {
    try {
      let rs: any = await this.ketStationService.select_hospcode(hospcode);
      this.itemeStation.push({ station_id: '', station_name: 'กรุณาเลือก' ,hospcode:''});
      rs.forEach((e: any) => {
        this.itemeStation.push(e);
      });
    } catch (error) {
      console.log('itemeStation',error);

    }
  }

  async getReferBack(i: any) {    

    try {
      let rs: any = await this.servicesService.view(i.hn, i.seq, i.referno,);

      if (rs) {

        this.allergy = rs.allergy;
        this.appointment = rs.appointment;
        this.diagnosis = rs.diagnosis;
        this.drugs = rs.drugs;
        this.hpi = rs.hpi[0];
        this.lab = rs.lab;
        this.medrecconcile = rs.medrecconcile;
        this.nurtures = rs.nurtures[0] || rs.nurtures;
        this.pe = rs.pe[0];
        this.procedure = rs.procedure;
        this.profile = rs.profile[0];
        this.refer = rs.refer[0];
        this.vaccines = rs.vaccines;
        this.xray = rs.xray;
        this.loads_id = this.refer.loads_id || '';
        this.station_id = this.refer.Station_ID || '';
        this.refer_type = this.refer.refer_type_back_id || '';
        this.cause_referback_id = this.refer.cause_referback_id || '';

        this.getStation(this.refer.to_hcode);
        
        this.expire_date = moment(new Date().getTime() + 365 * 86400E3).tz('Asia/Bangkok').format('YYYY-MM-DD')

        if (this.nurtures){
         
          this.diag_text = this.nurtures.diag_text;
          this.textCc = this.nurtures.cc;
          this.textPmh = this.nurtures.pmh;

          if (this.nurtures.weight && this.nurtures.height) {
            this.bmi = (this.nurtures.weight / ((this.nurtures.height / 100) * (this.nurtures.height / 100)));
          }
        }
        
        if (this.pe) {
          this.textPe = this.pe.pe;
        }
        if (this.hpi) {
          this.textHpi = this.hpi.hpi;
        }

        


        if (this.profile.chwpart && this.profile.amppart && this.profile.tmbpart) {
          if (this.profile.moopart && this.profile.moopart != "" && this.profile.moopart != "00") {
            this.getAddress_full(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart, this.profile.moopart);
          } else {
            this.getAddress(this.profile.chwpart, this.profile.amppart, this.profile.tmbpart);
          }
        }

        let refer_no_:any = `${this.refer.provider_code}-3-${this.refer.referno}`
        this.getAttachment(refer_no_);

      } else {


      }
    } catch (error) {
      console.log(error);
      this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');

    }

}
uploadsRoute(){
  let datas = this.itemStorage;
  let strdata:any = JSON.stringify(datas);
  sessionStorage.setItem('strdata',strdata);
  this.goToLink('/home/uploads');
}

goToLink(link:any) {
  const url = this.globalVariablesService.urlSite+link
  window.open(url, '_blank');
}

async onChangeSave(){
  this.blockedDocument = true;

  if(this.cause_referback_id && this.loads_id && this.station_id && this.refer_type ){
    this.onSave();
  }else{
    this.blockedDocument = false;

    this.alertService.error('เลือกข้อมูลไม่ครบ กรุณาเลือกข้อมูลใหม่');
  }
}

async onSave() {

  let dataPost:any;
  let refer_no_:any = `${this.refer.provider_code}-3-${this.refer.referno}`


  let typept_name:any;
  this.itemeTypept.forEach((e:any) => {
    if(e.typept_id == this.typept_id){
      typept_name = e.typept_name;
    }
  })

  let strength_name:any;
  this.itemeStrength.forEach((e:any) => {
    if(e.strength_id == this.strength_id){
      strength_name = e.strength_name;
    }
  })

  let loads_name:any;
  this.itemeLoads.forEach((e:any) => {
    if(e.loads_id == this.loads_id){
      loads_name = e.loads_name;
    }
  })

  let refer_type_name:any;
  this.itemeRefertype.forEach((e:any) => {
    if(e.refer_type == this.refer_type){
      refer_type_name = e.refer_type_name;
    }
  })

  let cause_referback_name:any;
  this.itemeReferCause.forEach((e:any) => {
    if(e.cause_referback_id == this.cause_referback_id){
      cause_referback_name = e.cause_referback_name;
    }
  })

  let station_name:any;
  this.itemeStation.forEach((e:any) => {
    if(e.station_id == this.station_id){
      station_name = e.station_name;
    }
  })
  let refer_triage_name:any;
  let location_refer_name:any;

  let diagnosis_ :any=[];
  let drugs_ :any=[];
  let medrecconcile_ :any=[];
  let allergy_ :any=[];
  let lab_ :any=[];
  let xray_ :any=[];
  let procedure_ :any=[];
  
  let _diagnosis_ :any=[];
  let _drugs_ :any=[];
  let _medrecconcile_ :any=[];
  let _allergy_ :any=[];
  let _lab_ :any=[];
  let _xray_ :any=[];
  let _procedure_ :any=[];

  _diagnosis_ = this.diagnosis || [];
  _drugs_ = this.drugs || [];
  _medrecconcile_ = this.medrecconcile || [];
  _allergy_ = this.allergy || [];
  _lab_ = this.lab || [];
  _xray_ = this.xray || [];
  _procedure_ = this.procedure || [];



  if(_diagnosis_[0]){
    this.diagnosis.forEach((e:any) => {
      let diagnosis = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "icd_code": e.icd_code,
        "icd_name": e.icd_name,
        "diag_type": e.diag_type,
        "diagnote": e.diagnote,
        "diagtype_id": e.diagtype_id
      }
      diagnosis_.push(diagnosis);

    });
  }

  if(_drugs_[0]){
    this.drugs.forEach((e:any) => {
      let drugs = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "seq": e.seq,
        "date_serv": e.date_serv,
        "time_serv": e.time_serv,
        "drug_name": e.drug_name,
        "qty": e.qty,
        "unit": e.unit,
        "usage_line1": e.usage_line1,
        "usage_line2": e.usage_line2,
        "usage_line3": e.usage_line3
      }
      drugs_.push(drugs);
    });
  }

  if(_medrecconcile_[0]){
    this.medrecconcile.forEach((e:any) => {
      let medrecconcile = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "drug_hospcode": e.drug_hospcode,
        "drug_hospname": e.drug_hospname,
        "drug_name": e.drug_name,
        "drug_use": e.drug_use,
        "drug_receive_date": e.drug_receive_date
      }
      medrecconcile_.push(medrecconcile);
    });
  }

  if(_allergy_[0]){
    this.allergy.forEach((e:any) => {
      let allergy = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "hname": e.hname,
        "drug_name": e.drug_name,
        "symptom": e.symptom,
        "begin_date": e.begin_date
      }
      allergy_.push(allergy);

    });
  }


  if(_lab_[0]){
    this.lab.forEach((e:any) => {
      let lab = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "date_serv": e.date_serv,
        "time_serv": e.time_serv,
        "labgroup": e.labgroup,
        "lab_name": e.lab_name,
        "lab_result": e.lab_result,
        "unit": e.unit,
        "standard_result": e.standard_result
      }
      lab_.push(lab);

    });
  }

  if(_xray_[0]){
    this.xray.forEach((e:any) => {
      let xray = {
        "refer_no": refer_no_,
        "xray_date": e.xray_date,
        "xray_name": e.xray_name
      }
      xray_.push(xray);

    });
  }

  if(_procedure_[0]){
    this.procedure.forEach((e:any) => {
      let procedure = {
        "hcode": e.provider_code,
        "refer_no": refer_no_,
        "seq": e.seq,
        "date_serv": e.date_serv,
        "time_serv": e.time_serv,
        "procedure_code": e.procedure_code,
        "procedure_name": e.procedure_name,
        "start_date": e.start_date,
        "end_date": e.end_date
      }
      procedure_.push(procedure);

    });
  }



  dataPost = {
    "hospital": {
      "providerCode": this.refer.provider_code,
      "providerName": this.refer.provider_name,
      "providerUser": this.username
    },
    "referback": {
      "hcode": this.refer.provider_code,
      "refer_no": refer_no_,
      "referout_no": "",
      "refer_date": this.today,
      "refer_time": this.totime,
      "hn": this.refer.hn,
      "cid": this.profile.cid,

      "pname": this.profile.title_name,
      "fname": this.profile.first_name,
      "lname": this.profile.last_name,
      "age": this.profile.age,
      "dob" : this.profile.brthdate,
      "father_name": this.profile.father_name,
      "mother_name": this.profile.mother_name,
      "couple_name": this.profile.couple_name,
      "contact_name": this.profile.contact_name,
      "contact_relation": this.profile.contact_relation,
      "contact_mobile": this.profile.contact_mobile,
      "addrpart": this.profile.addrpart,
      "moopart": this.profile.moopart,
      "tmbpart": this.profile.tmbpart,
      "amppart": this.profile.amppart,
      "chwpart": this.profile.chwpart,
      "doctor_id": this.refer.doctor,
      "doctor_name": this.refer.doctor_name,
      "cause_referback_id": this.refer_type,
      "cause_referback_name": refer_type_name,
      "refer_hospcode": this.refer.to_hcode,
      "referback_his_no": this.refer.referno,
      "refer_type_back_id": this.cause_referback_id,
      "refer_type_back_name": cause_referback_name,
      "refer_appoint": `${this.refer_appoint}`,
      "Station_ID": this.station_id,
      "Station_name": station_name,
      "Location_ID": this.refer.location_id,
      "Location_name": this.refer.location_name,
      "pttype_id": this.profile.pttype_id,
      "pttype_name": this.profile.pttype_name,
      "pttypeno": this.profile.pttype_no,
      "loads_id": this.loads_id,
      "loads_name": loads_name,
      "refer_remark": this.refer_remark,
      "is_coc":this.is_coc,
      "is_finish": "",
      "sex": this.profile.sex,
      "occupation": this.profile.occupation,
      "receive_no": "",
      "receive_date": "",
      "receive_time": "",
      "receive_refer_result": "",
      "receive_spclty_id": "",
      "receive_spclty_name": "",
      "receive_ward_id": "",
      "receive_ward_name": "",
      "receive_station_id": "",
      "receive_station_name": ""
    },
    "rfdiag": diagnosis_,
    "rfdrug": drugs_,
    "medrecconcile": medrecconcile_,
    "rfallergy": allergy_,
    "rflab": lab_,
    "xray": xray_,
    "rfprocedure": procedure_,
    "sign_text": [
      {
        "hcode": this.nurtures.provider_code || '',
        "refer_no": refer_no_,
        "body_weight_kg": this.nurtures.weight || '',
        "height_cm": this.nurtures.height  || '',
        "pds": this.nurtures.dbp  || '',
        "bps": this.nurtures.sbp  || '',
        "temperature": this.nurtures.temperature  || '',
        "hr": this.nurtures.pr  || '',
        "pulse": this.nurtures.pulse  || '',
        "rr": this.nurtures.rr  || '',
        "bloodgroup": this.nurtures.bloodgrp  || '',
        "cc": this.textCc,
        "pe": this.textPe,
        "hpi": this.textHpi,
        "pmh": this.textPmh,
        "treatment": this.nurtures.treatment || '',
        "oxygen_sat": this.nurtures.oxygen_sat || '',
        "eye_score": this.nurtures.eye_score || '',
        "movement_score": this.nurtures.movement_score || '',
        "vocal_score": this.nurtures.vocal_score || '',
        "pupil_right": this.nurtures.pupil_right || '',
        "pupil_left": this.nurtures.pupil_left || '',
        "diag_text": this.diag_text
      }
    ]
  }

  let info:any={};
  info.rows = dataPost;
  

  try {
    let rs:any = await this.ketReferbackService.onSeve(info)
    if(rs.res_.ok == true){
    this.blockedDocument = false;

      this.alertService.successPro('บันทึกสำเร็จเรียบร้อย','ดำเนินการเสร็จเรียบร้อย');
      this.router.navigate([sessionStorage.getItem('routmain')])
    }else{
    this.blockedDocument = false;

      console.log('error',rs.res_.error);
      if(rs.res_.error.code == 'ER_DUP_ENTRY'){
        this.alertService.error(`เลขรีเฟอร์ ${refer_no_} ถูกใช้งานส่งรีเฟอร์ไปแล้ว`,'บันทึกสำเร็จไม่สำเร็จ');
      }else{
        this.alertService.error(rs.res_.error.salMessage,'บันทึกสำเร็จไม่สำเร็จ');
      }
  }
    
  } catch (error) {
    this.blockedDocument = false;

    console.log('error',error);
    this.alertService.error(JSON.stringify(error),'ข้อมูลผิดพลาด');


    // this.alertService.error('ข้อมูลผิดพลาด','error');

  }

}


async getAddress_full(chwpart:any,amppart:any,tmbpart:any,moopart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select_full(chwpart,amppart,tmbpart,moopart);
    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

async getAddress(chwpart:any,amppart:any,tmbpart:any){
  try {
    let rs:any = await this.ketThaiaddressService.select(chwpart,amppart,tmbpart);
    this.textAddress = rs[0].full_name
  } catch (error) {
    console.log('error',error);
  }
}

expandall(){

  this.expander=true;
}

async getAttachment(i:any) {
  this.loadingAttachment = true;

  try {
    let rs: any = await this.ketAttachmentService.select(i);

    if (rs[0]) {
      this.itemeAttachment = rs;
      this.loadingAttachment = false;
    } else {
      this.loadingAttachment = false;
    }
  } catch (error) {
    console.log(error);
    this.loadingAttachment = false;
  }
  
}

async downloadAttRoute(filename: any){
  let download: any = await this.ketAttachmentService.download(filename);
}

async getCovidVaccine(i: any) {
  try {
    let rs: any = await this.ketReferbackService.covidvaccine(i);

    if(rs.statusCode == 200){
      this.itemeCovidVaccine = rs['info']['result']['vaccine_history'];
    }

  } catch (error) {
    console.log('ไม่พบรายการ');
  }
}


}
