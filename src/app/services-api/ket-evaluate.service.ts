import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetEvaluateService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async select() {
    const _url = `${this.apiUrl}/evaluate/select`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 
  
  async select_eva_detail(refer_no:any) {
    const _url = `${this.apiUrl}/evaluate/select_eva_detail/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async onSave(datas:any) {
    const _url = `${this.apiUrl}/evaluate/insert_eva_detail`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  } 

  async onUpdate(datas:any,refer_no:any) {
    const _url = `${this.apiUrl}/evaluate/update_eva_detail/${refer_no}`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  } 

  async select_eva_count(sdate:any,edate:any,hcode:any) {
    const _url = `${this.apiUrl}/evaluate/select_eva_count/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_eva_show(sdate:any,edate:any,hcode:any) {
    const _url = `${this.apiUrl}/evaluate/select_eva_show/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async select_eva_show_referin(sdate:any,edate:any,hcode:any) {
    const _url = `${this.apiUrl}/evaluate/select_eva_show_referin/${sdate}/${edate}/${hcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

}
