import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetBarthelRateService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async select() {
    const _url = `${this.apiUrl}/barthel_rate/select`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async select_brt_rate(brt_rate_id:any) {
    const _url = `${this.apiUrl}/barthel_rate/select_brt_rate?brt_rate_id=${brt_rate_id}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async select_cid(cid:any,refer_no:any) {
    const _url = `${this.apiUrl}/barthel_rate/select_cid?cid=${cid}&refer_no=${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async select_imc(imc_id:any) {
    const _url = `${this.apiUrl}/barthel_rate/select_imc?imc_id=${imc_id}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async onSave(datas:any) {
    const _url = `${this.apiUrl}/barthel_rate/insert`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  }  

  async onUpdate(datas:any,imc_id:any) {
    const _url = `${this.apiUrl}/barthel_rate/update?imc_id=${imc_id}`;
    return this.httpClient.put(_url,datas,this.httpOptions).toPromise();
  }  

  async onDelete(imc_id:any) {
    const _url = `${this.apiUrl}/barthel_rate/delete?imc_id=${imc_id}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

}
