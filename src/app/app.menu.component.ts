import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';
import { VersionService } from '../app/services-api/version.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-menu',
    styles: ['.menu-sidebar {color: white; }'],

    template: `
        <div class="layout-menu-container" >
            <ul class="layout-menu" role="menu" (keydown)="onKeydown($event)">
                <li app-menu class="layout-menuitem-category" *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true" role="none">
                    <div class="layout-menuitem-root-text" [attr.aria-label]="item.label" style="color: white;">{{item.label}}</div>
                    <ul role="menu">
                        <li app-menuitem *ngFor="let child of item.items" [item]="child" [index]="i" role="none" class="menu-sidebar" ></li>
                    </ul>
                </li>
                <a href="http://www.phoubon.in.th/#" target="_blank">
                    <img src="assets/layout/images/{{appMain.config.dark ? 'banner-primeblocks-dark' : 'banner-primeblocks'}}.png" alt="Prime Blocks" class="w-full mt-3"/>
                </a>
            </ul>
        </div>
    `
})
export class AppMenuComponent implements OnInit {

    model: any[];
    urlKet:any;
    urlReferboard:any = '#';
    urlCoc:any;
    pathName:any;

    constructor(
        public appMain: AppMainComponent,
        private versionService:VersionService,
        private location:Location,
        ) { 
            let x:any = this.location;
            this.urlCoc = x._locationStrategy._platformLocation.location.origin;
            this.pathName = x._locationStrategy._platformLocation.location.pathname;
        }

    ngOnInit() {
        this.getUrlReferboard();
        
    }

    async getUrlReferboard(){
        try {
            this.urlKet = await this.versionService.selectServer();
            // console.log(this.urlKet);
            if(this.urlKet[0]){
                this.urlReferboard = this.urlKet[0].api_url;
               
            }
            this. getMenu();
            } catch (error) {
            console.log(error);
        }
    }
    async getMenu(){

        this.model = [
            {
                label: 'Home',
                items: [
                    {
                        label: 'Referboard', icon: 'pi pi-chart-pie',
                        command: () => {
                            // this.update();
                        },
                        // routerLink: ['/dashboard'],
                        url: [`${this.urlReferboard}`], target: '_blank'
                    },
                    { label: 'Refer Out', icon: 'fas fa-shipping-fast', routerLink: ['/home/referout'] },
                    { label: 'Refer In', icon: 'fas fa-wheelchair', routerLink: ['/home/referin'] },
                    { label: 'Refer Back', icon: 'fas fa-ambulance', routerLink: ['/home/referback'] },
                    { label: 'Refer Receive', icon: 'fas fa-procedures', routerLink: ['/home/referreceive'] },
                    { label: 'Appoint', icon: 'fas fa-calendar-plus', routerLink: ['/home/appoint'] },  
                    { label: 'Appoint-slots', icon: 'pi pi-cog', routerLink: ['/home/appoint-slots'] },              
                    { label: 'Evaluate', icon: 'fab fa-angellist', routerLink: ['/home/evalate'] },
                    { label: 'Smart EMR', icon: 'pi pi-map-marker', url: `${this.urlCoc}${this.pathName}smartemr/` },
                    { label: 'Smart COC', icon: 'pi pi-check-circle', url: `${this.urlCoc}${this.pathName}coc/` },
                    { label: 'Report', icon: 'pi pi-chart-bar', routerLink: ['/home/report'] },
                    // { label: 'Helpdesk', icon: 'pi pi-chart-bar', routerLink: ['/home/helpdesk'] },
                ]
            },
          
            {
                label: 'Get Started',
                items: [
                    {
                        label: 'Documentation', icon: 'pi pi-fw pi-question', routerLink: ['/documentation']
                    },
                    {
                        label: 'Smarrefer Version', icon: 'pi pi-fw pi-question', routerLink: ['/home/version']
                    }
                ]
            }
        ];

    }
    onKeydown(event: KeyboardEvent) {
        const nodeElement = (<HTMLDivElement>event.target);
        if (event.code === 'Enter' || event.code === 'Space') {
            nodeElement.click();
            event.preventDefault();
        }
    }
}
